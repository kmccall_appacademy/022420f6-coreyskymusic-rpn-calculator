class RPNCalculator
  attr_accessor :stack
  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def plus
    perform_operation(:+)
  end

  def value
    @stack.empty? ? fails : @stack.last
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end


  def fails
    raise "calculator is empty"
  end


  def perform_operation(sym)
    fails if @stack.count < 2
    case sym
    when :+
      @stack << stack.pop + stack.pop
    when :-
      first = stack.pop
      @stack << stack.pop - first
    when :*
      @stack << stack.pop * stack.pop
    when :/
      first = stack.pop.to_f
      @stack << stack.pop.to_f / first
    end
  end


  def tokens(string)
    syms = ["+", "-", "*", "/"]
    string.split.map { |el| syms.include?(el) ? el.to_sym : el.to_i }
  end

  def evaluate(string)
    array = tokens(string)
    array.each do |el|
      case el
      when Integer
        push(el)
      else
        perform_operation(el)
      end
    end
    value
  end


end
